# How to Run HoloViews Demo Week 1
1. On your terminal, navigate to the desired repository in which you want to store this file.
2. Once there, paste the following line of code into your terminal:
    `git clone git@gitlab.oit.duke.edu:jca55/final-test-project-week-1.git` 
    then press return/enter. This will clone the repository into your device.
3. Open the "final-test-project-week-1" folder.
4. Click on the 'testing' folder and then open the 'demo1' folder.
5. Open the file called Week1Demo.ipynb.
6. Once the file is open, you can fully run it from your device.
    
   